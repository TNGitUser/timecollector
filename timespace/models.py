from django.db import models
from django.utils import timezone
from django.urls import reverse
from datetime import datetime, date, timedelta, time
from calendar import monthrange
import calendar
import uuid

# Create your models here.

class ObjectiveType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique id for the object')
    title = models.CharField(max_length=100)

    def __str__(self):
        """String for representing the Model object."""
        return f'{self.title}'

class Objective(models.Model):
    goal = models.IntegerField('Objective', default=140)
    type = models.ForeignKey('ObjectiveType', on_delete=models.SET_DEFAULT, default="Normal")

    def __str__(self):
        """String for representing the Model object."""
        return f'{self.goal}, {self.type}'

class Day(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique id for the day')
    ddate = models.DateField('Date', unique=True, default=timezone.now)
    start = models.TimeField('Arrived', default=timezone.now)
    end = models.TimeField('Departed', default=timezone.now)
    POSSIBLE_DAY = (
        ('Monday', 'Lundi'),
        ('Tuesday', 'Mardi'),
        ('Wednesday', 'Mercredi'),
        ('Thursday', 'Jeudi'),
        ('Friday', 'Vendredi'),
        ('Saturday', 'Samedi'),
        ('Sunday', 'Dimanche'),
    )

    day = models.CharField(
        max_length=20,
        choices=POSSIBLE_DAY,
        default=POSSIBLE_DAY[calendar.weekday(timezone.now().year, timezone.now().month, timezone.now().day)],
        help_text='Jour de la semaine',
    )
    class Meta:
        ordering = ['-ddate']

    def get_month(self):
        if self.date:
            return self.ddate.strftime("%B")

    def elapsed_time(self):
        duration = datetime.combine(date.min, self.end) - datetime.combine(date.min, self.start)
        return f'{duration}'

    def get_hours(self):
        t1 = time(self.start.hour, self.start.minute, self.start.second)
        t2 = time(self.end.hour, self.end.minute, self.end.second)
        #print(t1)
        #print(t2)
        duration = datetime.combine(date.min, t2) - datetime.combine(date.min, t1)
        #print(duration)
        return duration

    def __str__(self):
        """String for representing the Model object."""
        return f'{self.ddate}, {self.day}'

class Month(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique id for the day')
    YEAR_LIST = (
        ('2018', '2018'),
        ('2019', '2019'),
        ('2020', '2020'),
        ('2021', '2021'),
        ('2022', '2022'),
        ('2023', '2023'),
    )
    year = models.CharField(
        max_length=4,
        choices=YEAR_LIST,
        default=datetime.today().strftime("%Y"),
    )
    MONTH_LIST = (
        ('01', 'Janvier'),
        ('02', 'Février'),
        ('03', 'Mars'),
        ('04', 'Avril'),
        ('05', 'Mai'),
        ('06', 'Juin'),
        ('07', 'Juillet'),
        ('08', 'Août'),
        ('09', 'Septembre'),
        ('10', 'Octobre'),
        ('11', 'Novembre'),
        ('12', 'Décembre'),
    )
    month = models.CharField(
        max_length=2,
        choices=MONTH_LIST,
        default=datetime.today().strftime("%m"),
    )
    goal = models.ForeignKey('Objective', on_delete=models.SET_DEFAULT, default=1)
    days = []
    #days = [Day.objects.create(date = date(2019, 9, i)) for i in range(1, 31)]
    #days = models.ManyToManyField('Day', blank=True, limit_choices_to={'date__gte':f'{datetime.today().strftime("%Y")}-{datetime.today().strftime("%m")}-01', 'date__lte':f'{datetime.today().strftime("%Y")}-{datetime.today().strftime("%m")}-{monthrange(int(datetime.today().strftime("%Y")), int(datetime.today().strftime("%m")))}'})

    class Meta:
        ordering = ['year', 'month']

    def get_num_days(self):
        return (len(self.days))

    def add_day(self, elem):
        self.days.append(elem)

    def get_completion(self):
        return ('%.2f'%(((self.get_worked_hours().seconds / 3600) / self.goal.goal) * 100))

    def get_completion_chart(self):
        return ('%.2f'%(((self.get_worked_hours().seconds / 3600))))

    def get_remaining_chart(self):
        return (self.goal.goal - (((self.get_worked_hours().seconds / 3600))))

    def get_name(self):
        return f'{calendar.month_name[int(self.month)]}'

    def get_worked_hours(self):
        time = timedelta(days=0)
        for day in self.days:
            #print(day.get_hours())
            time += (day.get_hours())
        if (self.month == '09'):
            time = timedelta(seconds=3600 * 14)
        return time

    def get_absolute_url(self):
        """Returns the url to access a detail record for this month."""
        return reverse('detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return f'{self.month}, {self.year}'

#    def save(self, *args, **kwargs):
#        _, ndays = monthrange(int(self.year), int(self.month))
#        print(len(self.days))
#        if (len(self.days) == 0):
#            print('Adding days  ')
#            for i in range(1, ndays + 1):
#                print(Day.objects.get_or_create(ddate = date(int(self.year), int(self.month), i)))
                ##                #day = calendar.day_name[calendar.weekday(int(self.year), int(self.month), i)])[0])
#                self.days.append(Day.objects.get_or_create(ddate = date(int(self.year), int(self.month), i))[0])
#        print(self.month)
#        super().save(*args, **kwargs)
