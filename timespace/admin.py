from django.contrib import admin
from timespace.models import Day, Month, Objective, ObjectiveType

# Register your models here.

#admin.site.register(Objective)

@admin.register(Day)
class DayAdmin(admin.ModelAdmin):
    list_display = ('ddate', 'day', 'start', 'end', 'elapsed_time')

@admin.register(Month)
class MonthAdmin(admin.ModelAdmin):
    list_display = ('month', 'goal', 'year', 'get_num_days', 'get_worked_hours')

@admin.register(Objective)
class ObjectiveAdmin(admin.ModelAdmin):
    list_display = ('type', 'goal')

@admin.register(ObjectiveType)
class ObjectiveTypeAdmin(admin.ModelAdmin):
    list_display = ('title',)
