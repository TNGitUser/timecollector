from django.shortcuts import render
from django.http import HttpResponse

from timespace.models import Month
# Create your views here.

def index(request):
    """View function for home page of site."""

    month_data = Month.objects.all()

    context = {
        'months': month_data,
        'current': month_data[0]
    }
    # Render the HTML template index.html with the data in the context variable
    return render(request, 'index.html', context=context)

def detail(request, month):
    """View function for home page of site."""

    month_data = Month.objects.all()

    context = {
        'months': month_data,
        'current': Month.objects.get(id=month)
    }
    # Render the HTML template index.html with the data in the context variable
    return render(request, 'month_view.html', context=context)
    #return HttpResponse("You're looking at month %s." % Month.objects.get(id=month))

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)
